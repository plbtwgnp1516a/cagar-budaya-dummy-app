package com.example.herlin.dummyappplbtw;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

/**
 * Created by givan on 5/29/2016.
 */
public class ListOnClickActivity extends AppCompatActivity {
    private String nama,deskripsi,jenis,tahun,longitude,latitude;
    private Button btnMaps;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_click);

        Bundle b = getIntent().getExtras();
        b=getIntent().getExtras();
        nama=b.getString("nama");
        deskripsi=b.getString("deskripsi");
        jenis=b.getString("jenis");
        tahun=b.getString("tahun");
        latitude=b.getString("latitude");
        longitude=b.getString("longitude");

        TextView vv = (TextView) findViewById(R.id.textViewNama);
        TextView vv2 = (TextView) findViewById(R.id.textViewTahun);
       // TextView vv3 = (TextView) findViewById(R.id.textViewJenis);
        TextView vv1 = (TextView) findViewById(R.id.textViewDeskripsi);
        btnMaps=(Button) findViewById(R.id.buttonMaps);
        setTitle(nama);
        vv.setText(nama);
        vv2.setText(tahun);
       // vv3.setText(jenis);
        vv1.setText(deskripsi);

        btnMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putString("latitude",latitude);
                b.putString("longitude",longitude);
                Intent i = new Intent(ListOnClickActivity.this, MapsActivity.class);
                i.putExtras(b);
                startActivity(i);
            }
        });





    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
}
