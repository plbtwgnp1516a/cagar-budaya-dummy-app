package com.example.herlin.dummyappplbtw.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.example.herlin.dummyappplbtw.ListOnClickActivity;
import com.example.herlin.dummyappplbtw.MainActivity;
import com.example.herlin.dummyappplbtw.OnLoadMoreListener;
import com.example.herlin.dummyappplbtw.R;
import com.example.herlin.dummyappplbtw.ServiceGenerator;
import com.example.herlin.dummyappplbtw.api.ApiCagarList;
import com.example.herlin.dummyappplbtw.model.CagarModel;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

/**
 * Created by Herlin on 5/21/2016.
 */
public class RecyclerAdapterCagar extends RecyclerView.Adapter {
    private MainActivity mainActivity;
    public ApiCagarList apiCagarList = null;
    private final int TYPE_CAGAR = 1;
    private final int TYPE_LOAD_MORE = 2;
    private int lastVisibleItem, totalItemCount;
    private boolean loading;
    private OnLoadMoreListener onLoadMoreListener;
    private int visibleThreshold = 1;
    private Context context;
    private LayoutInflater inflater;
    private List<CagarModel> cagarModelList;
    private CoordinatorLayout coordinatorLayout;
    private CagarViewHolder cagarViewHolder;
    public void setCagarListActivity(MainActivity mainActivity) {
        this.mainActivity= mainActivity;
    }

    public void setContext(Context context) {
        this.context = context;
    }
    public void setOnLoadMoreListener(OnLoadMoreListener onLoadMoreListener) {
        this.onLoadMoreListener = onLoadMoreListener;
    }
    public void setInflater(LayoutInflater inflater) {
        this.inflater = inflater;
    }

    public void setLoaded() {
        loading = false;
    }
    public RecyclerAdapterCagar
            (List<CagarModel> cagarModelList, RecyclerView recyclerView) {
        apiCagarList = ServiceGenerator.createService(ApiCagarList.class);
        this.cagarModelList = cagarModelList;

        if (recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView
                    .getLayoutManager();

            recyclerView
                    .addOnScrollListener(new RecyclerView.OnScrollListener() {
                        @Override
                        public void onScrolled(RecyclerView recyclerView,
                                               int dx, int dy) {
                            super.onScrolled(recyclerView, dx, dy);

                            totalItemCount = linearLayoutManager.getItemCount();
                            lastVisibleItem = linearLayoutManager
                                    .findLastVisibleItemPosition();
                            if (!loading
                                    && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                                // End has been reached
                                // Do something
                                if (onLoadMoreListener != null) {
                                    onLoadMoreListener.onLoadMore();
                                }
                                loading = true;
                            }
                        }
                    });
        }

    }
    @Override
    public int getItemViewType(int position) {
        if(cagarModelList.get(position) != null)
            return TYPE_CAGAR;
        else
            return TYPE_LOAD_MORE;

    }
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        int layoutId;
        View view;
        switch (viewType) {
            case TYPE_CAGAR:
                layoutId = R.layout.item_cagar_list;
                view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
                return new CagarViewHolder(view);
            case TYPE_LOAD_MORE:
                layoutId = R.layout.progress_item;
                view = LayoutInflater.from(parent.getContext()).inflate(layoutId, parent, false);
                return new ProgressViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof CagarViewHolder)
            setCagarViewHolder(holder, position);
        else
            setProgressBarViewHolder(holder);
    }
    public void setProgressBarViewHolder(RecyclerView.ViewHolder holder) {
        ((ProgressViewHolder) holder).progressBar.setIndeterminate(true);
    }

    public void setCoordinatorLayout(CoordinatorLayout coordinatorLayout) {
        this.coordinatorLayout = coordinatorLayout;
    }


    public void setCagarViewHolder(RecyclerView.ViewHolder holder, final int position) {
        cagarViewHolder = (CagarViewHolder) holder;


        cagarViewHolder.cagarModel = cagarModelList.get(position);

        cagarViewHolder.textNamaCagar.setText(cagarViewHolder
                .cagarModel.getNama_cagar()+"");
        cagarViewHolder.textTahun.setText(cagarViewHolder
                .cagarModel.getTahun_peninggalan() + "");

        cagarViewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putString("nama",cagarModelList.get(position).getNama_cagar());
                b.putString("deskripsi", cagarModelList.get(position).getDeskripsi());
                b.putString("tahun", cagarModelList.get(position).getTahun_peninggalan());
                b.putString("jenis", cagarModelList.get(position).getId_jenis());
                b.putString("longitude",cagarModelList.get(position).getLongitude());
                b.putString("latitude",cagarModelList.get(position).getLatitude());
                Intent i = new Intent(v.getContext(),ListOnClickActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                i.putExtras(b);
                context.startActivity(i);
            }
        });

    }

    @Override
    public int getItemCount() {
        return (cagarModelList.size());
    }

    public static class CagarViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.textNamaCagar)
        TextView textNamaCagar;
        @Bind(R.id.textTahun)
        TextView textTahun;
        @Bind(R.id.imageCagar)
        ImageView imageCagar;

        CagarModel cagarModel;

        public CagarViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
    public static class ProgressViewHolder extends RecyclerView.ViewHolder {
        @Bind(R.id.progressBar1)
        ProgressBar progressBar;

        public ProgressViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, itemView);
        }
    }
}
