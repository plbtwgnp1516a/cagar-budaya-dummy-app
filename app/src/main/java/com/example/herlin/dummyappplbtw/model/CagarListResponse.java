package com.example.herlin.dummyappplbtw.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by VAIO on 5/14/2016.
 */
public class CagarListResponse {
    @SerializedName("content")
    private ArrayList<CagarModel> listCagar = new ArrayList<>();
    @SerializedName("totalPages")
    private int totalPages ;


    public ArrayList<CagarModel> getListCagar() {
        return listCagar;
    }

    public void setListCagar(ArrayList<CagarModel> listCagar) {
        this.listCagar = listCagar;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public void setTotalPages(int totalPages) {
        this.totalPages = totalPages;
    }
}

