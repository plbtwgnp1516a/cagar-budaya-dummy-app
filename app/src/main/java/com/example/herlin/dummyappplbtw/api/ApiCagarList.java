package com.example.herlin.dummyappplbtw.api;

import com.example.herlin.dummyappplbtw.model.CagarListResponse;

import retrofit.Call;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

/**
 * Created by Herlin on 5/21/2016.
 */
public interface ApiCagarList {
    @GET("cagarbudaya/{page}/{size}")
    Call<CagarListResponse>
    getHerritageList(@Path("page") String page, @Path("size") String size, @Query("api_key") String apiKey);
    // www.api.plbtw/cagarbudaya/1/5?api_key=asdfghjkl
}
