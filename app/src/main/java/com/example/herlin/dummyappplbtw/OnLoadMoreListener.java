package com.example.herlin.dummyappplbtw;

public interface OnLoadMoreListener {
    void onLoadMore();
}
