package com.example.herlin.dummyappplbtw.model;

/**
 * Created by VAIO on 5/14/2016.
 */
public class CagarModel {
    private String id_cagar;
    private String nama_cagar;//
    private String deskripsi;//
    private String tahun_peninggalan;//
    private String id_jenis;
    private String id_multimedia;
    private String id_user;
    private String id_desa;
    private String longitude;
    private String latitude;

    public String getId_cagar() {
        return id_cagar;
    }

    public void setId_cagar(String id_cagar) {
        this.id_cagar = id_cagar;
    }

    public String getNama_cagar() {
        return nama_cagar;
    }

    public void setNama_cagar(String nama_cagar) {
        this.nama_cagar = nama_cagar;
    }

    public String getDeskripsi() {
        return deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public String getTahun_peninggalan() {
        return tahun_peninggalan;
    }

    public void setTahun_peninggalan(String tahun_peninggalan) {
        this.tahun_peninggalan = tahun_peninggalan;
    }

    public String getId_jenis() {
        return id_jenis;
    }

    public void setId_jenis(String id_jenis) {
        this.id_jenis = id_jenis;
    }

    public String getId_multimedia() {
        return id_multimedia;
    }

    public void setId_multimedia(String id_multimedia) {
        this.id_multimedia = id_multimedia;
    }

    public String getId_user() {
        return id_user;
    }

    public void setId_user(String id_user) {
        this.id_user = id_user;
    }

    public String getId_desa() {
        return id_desa;
    }

    public void setId_desa(String id_desa) {
        this.id_desa = id_desa;
    }

    public String getLongitude() {return longitude;}

    public void setLongitude(String longitude){this.longitude=longitude;}

    public String getLatitude() {return latitude;}

    public void setLatitude(String latitude){this.latitude=latitude;}
}
