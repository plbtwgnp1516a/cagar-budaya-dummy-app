package com.example.herlin.dummyappplbtw;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.example.herlin.dummyappplbtw.adapter.RecyclerAdapterCagar;
import com.example.herlin.dummyappplbtw.api.ApiCagarList;
import com.example.herlin.dummyappplbtw.model.CagarListResponse;
import com.example.herlin.dummyappplbtw.model.CagarModel;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.Retrofit;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Herritage List");
        ButterKnife.bind(this);
        currentPage = 1;
        totalPages=0;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
               new LinearLayoutManager(getApplicationContext()));

        progressBar.setVisibility(View.VISIBLE);

        apiCagarList = ServiceGenerator
                .createService(ApiCagarList.class);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter == null) {
                    progressBar.setVisibility(View.VISIBLE);
                }
                currentPage = 1;
                getCagarList();
            }
        });
        getCagarList();
    }

    View rootview;
    public static final String API_KEY="mheTciX7IGbt3EZMNRw8";
    @Bind(R.id.progressBar)
    ProgressBar progressBar;
    @Bind(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    //  @Bind(R.id.contentCagarList)
    //  LinearLayout contentCagarList;

    @Bind(R.id.listCagar)
    RecyclerView recyclerView;
    @Bind(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;

    private List<CagarModel> cagarModelList;
    private ApiCagarList apiCagarList = null;
    private RecyclerAdapterCagar adapter;

    private int currentPage;
    private int totalPages;
    private final static int PAGE_SIZE = 10;

   /* public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        super.onCreate(savedInstanceState);
        rootview= inflater.inflate(R.layout.activity_cagar_list, container, false);
        getActivity().setTitle("Herritage List");
        ButterKnife.bind(this, rootview);
        currentPage = 1;
        totalPages=0;
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(
                new LinearLayoutManager(getActivity().getApplicationContext()));

        progressBar.setVisibility(View.VISIBLE);

        apiCagarList = ServiceGenerator
                .createService(ApiCagarList.class);


        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (adapter == null) {
                    progressBar.setVisibility(View.VISIBLE);
                }
                currentPage = 1;
                getCagarList();
            }
        });
        getCagarList();

        return rootview;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    */

    private void refreshPushNotification() {
        progressBar.setVisibility(View.VISIBLE);
        //   contentCagarList.setVisibility(View.INVISIBLE);
        getCagarList();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (9 == resultCode && 9 == requestCode) {
            refreshPushNotification();
            Snackbar.make(coordinatorLayout, "Push Notification Edited", Snackbar.LENGTH_LONG).show();
        }
    }

    private void getCagarList() {
        Call<CagarListResponse> call = null;
        call = apiCagarList.getHerritageList(currentPage+"",PAGE_SIZE+"",API_KEY);

//        call = apiReward.getHerritageList();
        call.enqueue(new Callback<CagarListResponse>() {
            @Override
            public void onResponse(Response<CagarListResponse> response,
                                   Retrofit retrofit) {

                if (2 == response.code() / 100 && response.isSuccess()) {
                    showCagarList(response);
                } else {
                    showErrorMessage();
                }
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                //         contentCagarList.setVisibility(View.VISIBLE);
            }

            @Override
            public void onFailure(Throwable t) {
                Snackbar.make(coordinatorLayout, "Failed to Load Content",
                        Snackbar.LENGTH_LONG).show();
                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                //        contentCagarList.setVisibility(View.VISIBLE);
            }
        });
    }

    private void showErrorMessage() {
        Snackbar.make(coordinatorLayout, "Error to Load Content", Snackbar.LENGTH_LONG).show();
    }

    private void showCagarList
            (Response<CagarListResponse> response) {

        final CagarListResponse cagarListResponse = response.body();

        totalPages=cagarListResponse.getTotalPages();
        cagarModelList = cagarListResponse.getListCagar();
//        Toast.makeText(getContext().getApplicationContext(), cagarListResponse.getListCagar().size()+"a", Toast.LENGTH_SHORT).show();
        adapter = new RecyclerAdapterCagar(cagarModelList, recyclerView);
        adapter.setCoordinatorLayout(coordinatorLayout);
        adapter.setContext(getApplicationContext());
        adapter.setInflater(getLayoutInflater());
        adapter.setCagarListActivity(this);
        recyclerView.setAdapter(adapter);

        if(totalPages!=currentPage) {
            adapter.setOnLoadMoreListener(new OnLoadMoreListener() {
                @Override
                public void onLoadMore() {
                    if (cagarModelList.get(cagarModelList.size() - 1) != null) {
                        cagarModelList.add(null);
                        adapter.notifyItemInserted(cagarModelList.size() - 1);
                    }

                    Call<CagarListResponse> call = null;
                    currentPage++;
                    call = apiCagarList.getHerritageList(currentPage + "", PAGE_SIZE + "", API_KEY);
                    call.enqueue(new Callback<CagarListResponse>() {
                        @Override
                        public void onResponse(Response<CagarListResponse> response,
                                               Retrofit retrofit) {
                            cagarModelList.remove(cagarModelList.size() - 1);
                            adapter.notifyItemRemoved(cagarModelList.size());
                            if (2 == response.code() / 100) {
                                loadMoreCagar(response);
                            } else {
                                showErrorMessage();
                                adapter.setLoaded();
                            }
                        }

                        @Override
                        public void onFailure(Throwable t) {
                            cagarModelList.remove(cagarModelList.size() - 1);
                            adapter.notifyItemRemoved(cagarModelList.size() + 1);
                            Snackbar.make(coordinatorLayout, "Failed to Load Content",
                                    Snackbar.LENGTH_LONG).show();
                            adapter.setLoaded();
                        }
                    });
                }
            });
            adapter.setLoaded();
        }
    }

    private void loadMoreCagar
            (Response<CagarListResponse> response) {
        CagarListResponse cagarListResponse = response.body();
        if (0 == cagarListResponse.getListCagar().size()) {
            Snackbar.make(coordinatorLayout, "No More Herritage", Snackbar.LENGTH_LONG).show();
        } else {
            for (CagarModel cagarModel :
                    cagarListResponse.getListCagar()) {
                cagarModelList.add(cagarModel);
                adapter.notifyItemInserted(cagarModelList.size());
            }
        }
        adapter.setLoaded();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
